import { BaristaNgPage } from './app.po';

describe('barista-ng App', function() {
  let page: BaristaNgPage;

  beforeEach(() => {
    page = new BaristaNgPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
