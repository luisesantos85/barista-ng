import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedService } from './shared.service';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    //SharedService
  ],
  declarations: [
    //SharedService
  ],
})
export class CommonServicesModule { }
