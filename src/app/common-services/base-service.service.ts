import { Injectable } from '@angular/core';
import { Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class BaseService {

  protected baseUrl: string = 'http://localhost:3000/api';

  constructor() { }

  protected getHeaders(){
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  protected extractData(res: Response) {
    let body = res.json();
    return body || { };
  }

  protected handleError (error: Response | any) {
    console.log('handle error');
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
