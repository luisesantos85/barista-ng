import {ModuleWithProviders} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'

import {AppComponent} from './app.component';
import {RecipesViewComponent} from './recipes/recipes-view/recipes-view.component';
import {BlogViewComponent} from './blog/blog-view/blog-view.component';
import {LocationsViewComponent} from './locations/locations-view/locations-view.component';
import {BrewViewComponent} from './brew/brew-view/brew-view.component';
import {HistoryViewComponent} from './history/history-view/history-view.component';
import {VideoViewComponent} from './videos/video-view/video-view.component';
import {MainViewComponent} from './main/main-view/main-view.component';

export const router: Routes = [
  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {path: 'main', component: MainViewComponent},
  {path: 'recipes', component: RecipesViewComponent},
  {path: 'blog', component: BlogViewComponent},
  {path: 'locations', component: LocationsViewComponent},
  {path: 'brew', component: BrewViewComponent},
  {path: 'videos', component: VideoViewComponent},
  {path: 'history', component: HistoryViewComponent},
]

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
