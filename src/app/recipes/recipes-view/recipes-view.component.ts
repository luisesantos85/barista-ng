import { Component, OnInit } from '@angular/core';
import { RecipesService } from "../recipes.service";
import { Recipe } from "../model/recipe";

@Component({
  selector: 'app-recipes-view',
  templateUrl: './recipes-view.component.html',
  styleUrls: ['./recipes-view.component.scss'],
  providers: [
    RecipesService
  ]
})
export class RecipesViewComponent implements OnInit {

  errorMessage: string;

  recipes: Array<Recipe> = [];

  description: string;

  constructor(private recipeService: RecipesService) { }

  ngOnInit() {
    this.getRecipes();
  }

  getRecipes() {
    this.recipeService.getAll()
      .subscribe(
        recipes => this.recipes = recipes,
        error =>  this.errorMessage = <any>error);
  }

  onRecipeSelected(option){
    this.showRecipeDetail(option);
  }

  showRecipeDetail(option){
    this.description = option.description;
  }
}
