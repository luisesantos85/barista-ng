export class Recipe {
  id: number;
  name: string;
  recipe: string;
}
