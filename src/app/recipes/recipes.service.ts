import { Injectable } from '@angular/core';
import { Recipe } from "./model/recipe";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { BaseService } from "../common-services/base-service.service";

@Injectable()
export class RecipesService extends BaseService{

  constructor(private http : Http){
    super();
  }

  getAll(): Observable<Recipe[]> {
    return this.http.get(`${this.baseUrl}/recipes`, {headers: super.getHeaders()})
      .map(super.extractData)
      .catch(super.handleError);
  }
}
