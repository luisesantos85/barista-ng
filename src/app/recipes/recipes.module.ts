import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipesViewComponent } from './recipes-view/recipes-view.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [RecipesViewComponent]
})
export class RecipesModule { }
