import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { SharedService } from '../../common-services/shared.service';

@Component({
  selector: 'main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss'],
  providers: [

  ]
})
export class MainViewComponent implements OnInit {

  constructor( private _sharedService: SharedService) { }

  ngOnInit() {
  }

  isOptionSelected(option){
      this._sharedService.emitChange(option);
  }
}
