import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainViewComponent } from './main-view/main-view.component';
import { CommonComponentsModule } from '../common-components/common-components.module';
import { CommonServicesModule } from '../common-services/common-services.module';

@NgModule({
  imports: [
    CommonModule,
    CommonComponentsModule,
    CommonServicesModule
  ],
  exports:[

  ],
  declarations: [
    MainViewComponent
  ]
})
export class MainModule { }
