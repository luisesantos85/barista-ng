import { Injectable } from '@angular/core';
import { Brew } from "./model/brew";
import {Http, RequestOptions} from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { BaseService } from "../common-services/base-service.service";


@Injectable()
export class BrewService extends BaseService{

  constructor(private http : Http){
    super();
  }

  getAll(): Observable<Brew[]> {
    return this.http.get(`${this.baseUrl}/brews`, {headers: super.getHeaders()})
      .map(super.extractData)
      .catch(super.handleError);
  }

  save(brew:Brew){
    let headers = super.getHeaders();
    let options = new RequestOptions({ headers: headers });
    return this.http.post(`${this.baseUrl}/brews`, brew, options)
      .map(super.extractData)
      .catch(super.handleError);
  }
}
