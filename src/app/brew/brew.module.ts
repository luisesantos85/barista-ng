import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrewViewComponent } from './brew-view/brew-view.component';
import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [BrewViewComponent]
})
export class BrewModule { }
