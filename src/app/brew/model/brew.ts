export class Brew {
  id: number;
  coffee: string;
  time: string;
  weight: number;
  unit: string;
  tool: string;

  constructor(coffee:string, time:string, weight:number, unit: string, tool:string){
    this.coffee = coffee;
    this.time = time;
    this.weight = weight;
    this.unit = unit;
    this.tool = tool;
  }
}
