import { Component, OnInit } from '@angular/core';
import {Brew} from "../model/brew";
import {BrewService} from "../brew.service";
import * as d3 from 'd3-selection';
import * as d3Scale from "d3-scale";
import * as d3Array from "d3-array";
import * as d3Axis from "d3-axis";

@Component({
  selector: 'app-brew-view',
  templateUrl: './brew-view.component.html',
  styleUrls: ['./brew-view.component.scss'],
  providers: [
    BrewService
  ]
})
export class BrewViewComponent implements OnInit {

  brew:Brew = new Brew("","2017-07-03T20:07:07.701Z",10,"gr","");

  brews: Array<Brew>=[];

  errorMessage: string;

  private width: number;
  private height: number;
  private margin = {top: 20, right: 20, bottom: 30, left: 40};

  private x: any;
  private y: any;
  private svg: any;
  private g: any;

  constructor(private brewService: BrewService) { }

  ngOnInit() {
    this.getBrews();
    this.initSvg()
  }

  getBrews(){
    var me = this;
    this.brewService.getAll()
      .subscribe(
        function(brews){
          me.brews = brews;
          me.initAxis();
          me.drawAxis();
          me.drawBars();
        },
        error =>  this.errorMessage = <any>error);
  }

  saveBrew(){
    var me = this;
    me.brewService.save(this.brew)
      .subscribe(
        function(brew){
          me.cleanForm();
          me.getBrews();
        },
        error =>  this.errorMessage = <any>error);
  }

  cleanForm(){
    this.brew.coffee = "";
    this.brew.tool = "";
  }

  private initSvg() {
    this.svg = d3.select("svg");
    this.width = +this.svg.attr("width") - this.margin.left - this.margin.right ;
    this.height = +this.svg.attr("height") - this.margin.top - this.margin.bottom;
    this.g = this.svg.append("g")
      .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");;
  }

  private initAxis() {
    this.x = d3Scale.scaleBand().rangeRound([0, this.width]).padding(0.1);
    this.y = d3Scale.scaleLinear().rangeRound([this.height, 0]);
    this.x.domain(this.brews.map((d) => d.coffee));
    this.y.domain([0, 15]);
  }

  private drawAxis() {
    this.g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3Axis.axisBottom(this.x));
    this.g.append("g")
      .attr("class", "axis axis--y")
      .call(d3Axis.axisLeft(this.y).ticks(10, ""))
      .append("text")
      .attr("class", "axis-title")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Frequency");
  }

  private drawBars() {
    this.g.selectAll(".bar")
      .data(this.brews)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("x", (d) => this.x(d.coffee) )
      .attr("y", (d) => this.y(d.weight) )
      .attr("width", this.x.bandwidth())
      .attr("height", (d) => this.height - this.y(d.weight) );
  }
}
