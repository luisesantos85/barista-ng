import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import {Comment} from "../model/comment";
export interface ConfirmModel {
  title:string;
  comments:Array<Comment>;
}
@Component({
  selector: 'confirm',
  template: `<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" (click)="close()" >&times;</button>
        <h4 class="modal-title">{{title || 'Confirm'}}</h4>
      </div>
      <!--<div class="modal-body">
        <p>{{message || 'Are you sure?'}}</p>
      </div>-->
      <div>
        <ul class="br-comment br-container">
          <li *ngFor="let comment of comments">
            <div class="br-entry-container">
              {{comment.content}}
            </div>
          </li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" (click)="close()" >Cancel</button>
      </div>
    </div>
  </div>`
})
export class CommentsModalComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
  title: string;
  comments: Array<Comment>;

  constructor(dialogService: DialogService) {
    super(dialogService);
  }
  confirm() {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code
    this.result = true;
    this.close();
  }
}
