import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {BaseService} from "../common-services/base-service.service";
import {Blog} from "./model/blog";
import {Entry} from "./model/entry";
import {Comment} from "./model/comment";

@Injectable()
export class BlogService extends BaseService{

  constructor(private http : Http){
    super();
  }

  getBlogs(): Observable<Blog[]> {
    return this.http.get(`${this.baseUrl}/blogs`, {headers: super.getHeaders()})
      .map(super.extractData)
      .catch(super.handleError);
  }

  getEntries(blogId): Observable<Entry[]> {
    return this.http.get(`${this.baseUrl}/blogs/${blogId}/entries`, {headers: super.getHeaders()})
      .map(super.extractData)
      .catch(super.handleError);
  }

  getComments(entryId): Observable<Comment[]> {
    console.log('get comments');
    return this.http.get(`${this.baseUrl}/entries/${entryId}/comments`, {headers: super.getHeaders()})
      .map(super.extractData)
      .catch(super.handleError);
  }
}
