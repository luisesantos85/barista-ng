export class Entry {
  id: number;
  title: string;
  content: string;
}
