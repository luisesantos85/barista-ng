export class Blog {
  id: number;
  name: string;
  description: string;
}
