import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Entry} from "../model/entry";
import {BlogService} from "../blog.service";
import {Blog} from "../model/blog";
import {Comment} from "../model/comment";
import {CommentsModalComponent} from "../comments-modal/comments-modal.component";
import {DialogService} from "ng2-bootstrap-modal";

@Component({
  selector: 'app-blog-view',
  templateUrl: './blog-view.component.html',
  styleUrls: ['./blog-view.component.scss'],
  providers: [
    BlogService
  ]
})
export class BlogViewComponent implements OnInit {

  errorMessage: string;
  entries: Array<Entry> = [];

  constructor(private blogService: BlogService, private dialogService:DialogService) { }

  ngOnInit() {
    this.getBlogs();
  }

  getBlogs() {
    var me = this;
    this.blogService.getBlogs()
      .subscribe(function(blogs){
        if(blogs.length > 0){
          let blog = blogs[0];
          me.blogService.getEntries(blog.id)
            .subscribe(
              entries => me.entries = entries,
              error =>  me.errorMessage = <any>error
            );
        }
      }, function(error){
        this.errorMessage = error;
      });
  }

  getComments(entryId){
    var me = this;
    this.blogService.getComments(entryId)
      .subscribe(function(comments){
          me.showCommentsWindow(comments);
      },
        function(error){
          console.log(error);
        }
      );
  }

  showCommentsWindow(comments) {
      this.dialogService.addDialog(CommentsModalComponent, {
      title:'Comments',
      comments:comments})
  }

  onEntrySelected(entryId){
    this.getComments(entryId);
  }
}
