import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogViewComponent } from './blog-view/blog-view.component';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { CommentsModalComponent } from './comments-modal/comments-modal.component';

@NgModule({
  imports: [
    CommonModule,
    BootstrapModalModule
  ],
  declarations: [
    BlogViewComponent,
    CommentsModalComponent
  ],
  entryComponents: [
    CommentsModalComponent
  ],
})
export class BlogModule { }
