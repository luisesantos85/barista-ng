import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationsViewComponent } from './locations-view/locations-view.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [LocationsViewComponent]
})
export class LocationsModule { }
