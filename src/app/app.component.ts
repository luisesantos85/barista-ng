import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationBarComponent } from './common-components/navigation-bar/navigation-bar.component';
import { ImageSliderComponent } from './common-components/image-slider/image-slider.component';
import { OptionsPanelComponent } from './common-components/options-panel/options-panel.component';
import { SharedService } from './common-services/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  entryComponents: [
    NavigationBarComponent,
    ImageSliderComponent,
    OptionsPanelComponent
  ],
  providers: [
  ]
})
export class AppComponent {

  router: Router;

  constructor( private _sharedService: SharedService, _router: Router) {
    this.router = _router;

    _sharedService.changeEmitted$.subscribe(
      route => {
        this.router.navigateByUrl('/' + route);
      });
  }
}
