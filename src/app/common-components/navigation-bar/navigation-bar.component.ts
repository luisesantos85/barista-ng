import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'navigation-bar-component',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss'],

})
export class NavigationBarComponent implements OnInit {

  name = 'navigation bar!';

  constructor() { }

  ngOnInit() {
  }

}
