import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'options-panel-component',
  templateUrl: './options-panel.component.html',
  styleUrls: ['./options-panel.component.scss']
})
export class OptionsPanelComponent implements OnInit {

  @Output() optionSelected: EventEmitter<boolean> = new EventEmitter<boolean>();

  name: String = "Options Panel";

  constructor() {

  }

  ngOnInit() {
  }

  onOptionClicked(option):void{
    this.optionSelected.next(option);
  }
}
