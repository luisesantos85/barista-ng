import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { ImageSliderComponent } from './image-slider/image-slider.component';
import { OptionsPanelComponent } from './options-panel/options-panel.component';
import { CarouselModule } from 'ngx-bootstrap';
//import { ModalComponent } from './modal/modal.component';

@NgModule({
  imports: [
    CommonModule,
    CarouselModule.forRoot()
  ],
  exports:[
    NavigationBarComponent,
    ImageSliderComponent,
    OptionsPanelComponent
  ],
  declarations: [
    NavigationBarComponent,
    ImageSliderComponent,
    OptionsPanelComponent,
    //ModalComponent
  ]
})
export class CommonComponentsModule { }
