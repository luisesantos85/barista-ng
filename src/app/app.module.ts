import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SharedService } from './common-services/shared.service';
import { CommonServicesModule } from './common-services/common-services.module';

import { AppComponent } from './app.component';

import { MainModule } from "./main/main.module";
import { CommonComponentsModule } from './common-components/common-components.module';
import { RecipesModule } from './recipes/recipes.module';
import { BlogModule } from './blog/blog.module';
import { BrewModule } from './brew/brew.module';
import { LocationsModule } from "./locations/locations.module";
import { HistoryModule } from "./history/history.module";
import { VideosModule } from "./videos/videos.module";

import {routes} from './app.router';



@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CommonComponentsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    routes,
    MainModule,
    RecipesModule,
    BlogModule,
    BrewModule,
    VideosModule,
    HistoryModule,
    LocationsModule,
    CommonServicesModule
  ],
  providers: [
    SharedService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
